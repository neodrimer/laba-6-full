import java.util.Scanner;
public class Task3 {
        public static void main(String[] args)
        {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Введите первое число");
            int a = scanner.nextInt();
            System.out.println("Введите второе число");
            int b = scanner.nextInt();
            System.out.println("Введите третье число");
            int c = scanner.nextInt();
            int nod = lcm(a,b,c);
            System.out.println("Наименьшее общее кратное трех чисел: " + nod);
        }
        public static int lcm(int a, int b, int c)
        {
            return lcm(lcm(a, b), c);
        }

        public static int lcm(int a, int b)
        {
            return a * b / gcd(a, b);
        }

        public static int gcd(int a, int b, int c)
        {
            return gcd(gcd(a, b), c);
        }
        public static int gcd(int a, int b)
        {
            while (b != 0)
            {
                int temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }
    }


