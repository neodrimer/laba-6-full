import java.util.Scanner;
public class Task4 {
    public static void main(String[] args)
    {
        int result = SumF();
        System.out.println("Факторила от 1 до 9: " + result);
    }
    public static int SumF()
    {
        int sum = 1;
        for (int i = 3; i <= 9; i += 2)
        {
            int factorial = 1;
            for (int j = i; j > 1; j--)
            {
                factorial *= j;
            }
            sum += factorial;
        }
        return sum;
    }
}