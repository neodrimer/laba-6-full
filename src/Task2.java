import java.util.Scanner;

public class Task2 {
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Эта прогарамма считает НОД 4 введенных вами чисесл");
        System.out.println("Введите первое число");
        int a = scanner.nextInt();
        System.out.println("Введите второе число");
        int b = scanner.nextInt();
        System.out.println("Введите третье число");
        int c = scanner.nextInt();
        System.out.println("Введите четвертое число");
        int d = scanner.nextInt();
        int nod = NOD(a,b,c,d);
        System.out.println("Наибольший общий делитель четырех чисел: " + nod);
    }
    public static int NOD(int a, int b)
    {
        while (b != 0)
        {
            int temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }
    public static int NOD(int a, int b,int c, int d)
    {
        return NOD(NOD(a,b),NOD(c,d));
    }
}


