import java.util.Scanner;

public class Task1 {
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите первое число");
        int a = scanner.nextInt();
        System.out.println("Введите второе число");
        int b = scanner.nextInt();
        int nod = NOD(a,b);
        System.out.println("Наибольший общий делитель двух чисел: " + nod);
        int nok = NOK(a,b);
        System.out.println("Наибольшее общее кратное двух чисел: " + nok);
    }
    public static int NOD(int a, int b )
    {
        while (b != 0)
        {
            int temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }
    public static int NOK(int a,int b )
    {
        return a * b / NOD(a, b);
    }
}

